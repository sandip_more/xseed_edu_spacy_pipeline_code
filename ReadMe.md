# 1. Checkout code from bitbucket.
Create and go to working directory

1. Take code from bitbucket *`git pull https://sandip_more@bitbucket.org/sandip_more/xseed_edu_code.git`*

2. Check status of code *`git status`*

3. Add changes in code (Use this command when no changes in code) *`git add .`*

4. Commit code (If you initiate step III then use this command) *`git commit -m "Commit name"`*

5. Add tag for commit *`git tag -a "v1" -m "v1 tag added"`*
  
# 2. DVC commands.
1. Push dataset to remote directory *`dvc push`*

# 3. Create virtual environment from requirement.txt file.
1. Create virtual enrironment *`python -m venv VENV`*

2. Activate virtual enrironment *`.\VENV\Scripts\activate`*

3. Install packages mentioned in requirements.txt file *`pip install -r requirements.txt`*

# 4. Run pipeline
1. Initiate to run pipeline *`spacy project run all`*
