import re
import datetime
from datetime import date
import docx2txt
import glob
import os

# Import pandas
import pandas as pd


# Get dvc remote url and read data

import dvc.api
edu_path = r"assets\educations.xlsx"
uni_path = r"assets\university_name.xlsx"
repo = r"."
version = "v1"

edu_url = dvc.api.get_url(
    path=edu_path,
    repo=repo,
    rev=version
)
uni_url = dvc.api.get_url(
    path=uni_path,
    repo=repo,
    rev=version
)

## Load the xlsx file
# excel_data = pd.read_excel(r"D:\tCognition\XSeed Machine Learning\Spacy Pipeline\XSeed ML Pipeline\assets\university_name.xlsx")
uni_excel_data = pd.read_excel(uni_url)

# Read the values of the file in the dataframe
data = pd.DataFrame(uni_excel_data)

universitys = []
for row in data.itertuples():
    b = (row[1].strip()+' '+row[2].strip())
    b = b.strip()
    universitys.append(b)

import pandas as pd
import random
import re

# Load the xlsx file
# excel_data = pd.read_excel(r"D:\tCognition\XSeed Machine Learning\Spacy Pipeline\XSeed ML Pipeline\assets\educations.xlsx")
edu_excel_data = pd.read_excel(edu_url)

# Read the values of the file in the dataframe
edu_data = pd.DataFrame(edu_excel_data)
edus = []
for row in edu_data.itertuples():
    u_l = (row[1])
    edus.append(u_l)

educations = []
for i in edus:
    # for ignore (text)
    plain_edu = re.sub("[\(\[].*?[\)\]]", "", i)
    # \xa0 is unwanted space char. which is appears when we pest data
    plain_edu = plain_edu.replace('\xa0', ' ')
    plain_edu = plain_edu.strip()
    plain_edu = plain_edu.replace("  ", " ")
    educations.append(plain_edu)
educations = list(set(educations))

# Garbage data

import random
garbage_data = [
'Habits of Highly Effective People Certified, Project Manager | Wipro Technologies Certified',
'Sigma Green Belt Certification PMI PMP Certified Information Technology Infrastructure Library',
'SKILLS Microsoft Project Server, Java, MS Visioal',
'Certified Scrum Master® from Scrum Alliance License No: 000491155 Certified',
'Scrum Alliance QTP/QC: Certifications on QTP and QC (Quality Center) from HP ISTQB: Certification',
'Certified Scrum Master (CSM) – License # 000466935 CORE COMPETENCIES SLA Management Testing/QA/Rollou',
'Certification (2000) ~ TQMI Internal Auditor IS0 9001:2000 ~ Soft skills development PROFESSIONAL EXPERI',
'Certifications, AWARDS and Achievements: [1Z0-591] OBIEE 11g',
'Certified PRINCE2 Practitioner (APMG), ID: P2R/IN076432 Certified Managing Multiple Projects',
'Certification from International Scrum Institute Professional Scrum master I (PSM I)',
'CERTIFICATIONS Project Management professional (PMI-PMP), Certification',
'Certified Scrum Master (International Scrum Institute) http://www.scrum-institute.org/certifications/Sc',
'TECHNICAL SKILLS: Programming Languages: Java/J2EE, PL/SQL, Unix Shell Scripts Java/J2EE Technologies: JavaBeans, collections, Servlets, JSP, JDBC, JNDI, RMI, EJB',
'PROFESSIONAL EXPERIENCE Client: Capital One Mar 17 – Till date Location: Mclean, VA Role:  Full Stack Java Developer',
'PROFESSIONAL EXPERIENCE Client: Capital One Mar 17 – Till date location: Mclean, VA Role:  Full Stack Java Developer',
'Career Progression Project Management Institute (PMI), Savannah Chapter Savannah, GA 12/2016 – Present Project Manager/VP of Volunteers',
'Career Progression Project Management Institute (PMI), Savannah Chapter Savannah, GA 12/2016 – Present Project Manager/VP of Volunteers',
'Certified Scrum Master (CSM). ISTQB certified tester. HP QC (Quality Center) and QTP (Quick Test Professional) Certified. Certified professional in Banking and Financial Management, Certified professional in Investment Management.NCFM Certified, Six Sigma Yellow Belt certification, Award of Star of the Month and Client appreciations.',
'Certifications: Professional Scrum Master (PSM). Scrum Master Accredited Certification (SMAC). Six Sigma Green Belt certified.',
'Professional Work Experience: Client:  JPMorgan Chase	MARCH 2016 to Till Date Location: Wilmington, Delaware. Role: Sr. Business Systems Analyst/ Scrum Master',
'Professional Development: Certificate of Project Management from edX & University of Adelaide (August, 2017)Certificate of Google Analytics Platform Principles from Google (March, 2014)',
'Professional Development: Certificate of Project Management from edX & University of Adelaide (August, 2017)Certificate of Google Analytics Platform Principles from Google (March, 2014)',
'CERTIFICATION: Project Management Professional (PMP) – Project Management Institute PMP Number: 1693785 PMP Expiration Date: February 6th 2020',
'TECHNICAL SKILLS: Languages C, C++, JAVA/J2EE, PL/SQL, Shell script, UNIX commands.',
'Work Experience: Onsite work experience: October 2015 to Present Senior Java Developer FPL/Infosys – Jupiter, Florida – February 2017 to present Description: This project which has been developed on PowerBI  tool',
'Certifications and Tools: Certified Professional Scrum Master™ level I (PSM I) JIRA, Confluence, Microsoft Project, MS Visio, Microsoft Office PROFESSIONAL EXPERIENCE',
'Professional Development Certified Professional Scrum Master (PSM) Certified Project Management Professional (PMP) Agile Project',
'PROFESSIONAL EXPERIENCE: Client: State Government of New York, Albany, New York Nov 2015- Till Date Title: Sr. Java/UI developer',
'Certifications: Scrum Master Certification from International Scrum Institute Professional Scrum master I (PSM I) Certification',
"PROJECTS EXPERIENCE Client: McDonald's, Oak Brook, IL   (HCL America) Feb 2017 - Present Project: Foundation Services (GLS, ConfigServices) Technologies: Mulesoft, Cloudhub",
'PROFESSIONAL EXPERIENCE Capital One Inc, Mclean, VA June 2016 to July 2017 Sr. Business and Data Analyst The Project ',
'TECHNICAL SKILLS: Modeling Tools Rational Requisite Pro, HPALM, UML, MS Visio, BPMN, Rational Rose, Rational Clear Case Methodologies / Frameworks Agile, Waterfall Database MS SQL Server, Oracle, MYSQL.',
'PROFESSIONAL EXPERIENCE Client: Citi Group- Tampa, FL (Sep 2016- Till date) Title: Sr. Business Analyst',
'PROFESSIONAL EXPERIENCE Client: Citi Group- Tampa, FL (Sep 2016- Till date) Title: Sr. Business Analyst',
'PROFESSIONAL EXPERIENCE: Walmart Labs, Sunnyvale, CA June 2017 – till date Innominds Software INC Sr. Java Developer Responsibilities:',
'WORK EXPERIENCE: Cigna – Bloomfield, Connecticut Jul’17 – Present Role: Hadoop/Spark Developer Responsibilities: Developed Spark',
'RECOGNITION & CERTIFICATIONS PMP – PMI (2017/09) IFRS – ACCA, UK (2014/07) SIX SIGMA – Green Belt (2016-17), LEAN (2014-17).',
'Professional Experience: Verizon, TX. Dec’16 – Tilldate. Role : Java Developer. Description :  Verizon DVS',
'TECHNICAL SKILLS Development / Service Technologies/ClientSide Technologies',
'Professional SCRUM Master (PSM) in agile methodology (2017) Scrum.org',
'Certification: Certified Data Scientist i.e. Data Science Professional (from John Hopkins University). Technical Skills: Windows',
'Volunteer: International Student Ambassador at New York Institute of Technology, Student Government Association.',
'PMI-Certified Project Management Professional (PMP) Six Sigma Green Belt',

 'PMI-Certified Project Management Professional (PMP) Six Sigma Green Belt The George Washington University School of Business',
 'SKILLS ▪ Proficient with MS Project, PPM, Smartsheet, Microsoft Word, Excel, PowerPoint, Outlook,',
 'projects. Strong experience in Agile Testing concepts and Testing process, experience in managing complex issues and risks,',
 'Certifications: Professional Scrum Master (PSM). Scrum Master Accredited Certification (SMAC). Six Sigma Green Belt certified.',
 'Certifications: Professional Scrum Master (PSM). Scrum Master Accredited Certification (SMAC). Six Sigma Green Belt certified.',
 'Professional Experience: PNC Bank, Pittsburg, PA October 2016 – Current Sr. Business Systems Analyst Digital Loan Process System The project',
 'GPA– 3.56 Professional Experience: Fifth Third Bank, Cincinnati, OH Mar 2015 to Present Senior Business Analyst/QA Lead Fifth Third Bank',
 'CERTIFICATION: Project Management Professional (PMP) – Project Management Institute PMP Number: 1693785 PMP Expiration Date: February 6th 2020',
 'Technical Skills: Languages C, C++, JAVA/J2EE, PL/SQL, Shell script, UNIX commands. Java Technologies Core Java, J2EE, JSP, Servlet, JDBC,',
 'IT SKILLS Design and Planning MS Project and Visio Languages: COBOL, SQL, REXX, IMS DC/DB, CICS, JCL, HTML, XML, Java, Java Script Databases',
 'Work Experience: Onsite work experience: October 2015 to Present Senior Java Developer FPL/Infosys – Jupiter, Florida – February 2017',
 'Certifications and Tools: Certified Professional Scrum Master™ level I (PSM I) JIRA, Confluence, Microsoft Project, MS Visio, Microsoft Office PROFESSIONAL EXPERIENCE 11',
 "PROFESSIONAL EXPERIENCE: Client: State Government of New York, Albany, New York Nov 2015- Till Date Title: Sr. Java/UI developer",
 'Certifications: Scrum Master Certification from International Scrum Institute Professional Scrum master I (PSM I) Certification',
 "PROJECTS EXPERIENCE Client: McDonald's, Oak Brook, IL (HCL America) Feb 2017 - Present Project: Foundation Services (GLS, ConfigServices)",
 'Professional Summary Business Analyst skilled in defining strategy and driving teams forward in the consolidation of data and business systems.',
 'Modeling Tools Rational Requisite Pro, HPALM, UML, MS Visio, BPMN, Rational Rose, Rational Clear Case Methodologies / Frameworks Agile,',
 'Personal data Gender Male Nationality Indian Address 16641 N 56 St , APT 1067, Scottsdale ,AZ -85254 Personal e-mail ID emailbalajig@gmail.com ',
 'PROFESSIONAL EXPERIENCE: Walmart Labs, Sunnyvale, CA June 2017 – till date Innominds Software INC Sr. Java Developer Responsibilities:',
 'WORK EXPERIENCE: Cigna – Bloomfield, Connecticut Jul’17 – Present Role: Hadoop/Spark Developer Responsibilities: Developed Spark ',
 'Visakhapatnam Negotiate to Win | 7 Habits of Highly Effective People Certified, Project Manager | Wipro Technologies Certified,',
 'PROFESSIONAL EXPERIENCE: Valspar, Mineapolis, MN Sept 2016 to Tilldate Role: Sr.JAVA UI developer Project: Valspar Description:',
 'Professional Diploma in Network Centered Computing St. Xavier’s College, Kolkata, India Economics References *Reference can be provided upon request.',
 'CERTIFICATIONS: ISTQB Certified Tester-Foundation Level (ASTQB board certification)',
 'Project Management Certification - Kent State University (PMP) Certified Scrum Master from',
 'Technical Skills: Programming Languages Java, C, C++. GUI Development Technologies Java AWT/Swing. Web Application',
 'Technical Skills: Languages : Java Spring Framework : Spring Boot, Cloud, Data, Restful Service, Security, Integration ORM Technologies',
 'Diploma in Advance Computing (C-DAC) Certifications: PMI - PMP, CSM (Certified Scrum Master), APICS – CSCP, LSSGB (Lean Six Sigma Green Belt)',
 'Professional Experience: Signet Jewelers, Fairlawn, OH – Oct 2017 - Till Date Currently involved in multiple retail projects and managing various stakeh',
 'Class of 2009 PROFESSIONAL EXPERIENCE: Citizens Energy Group - Indianapolis, IN Jan’17 - Till Date Senior Full Stack Java Developer RESPONSIBILITIES:',
 'Certification: Certified Data Scientist i.e. Data Science Professional (from John Hopkins University).',
 'Technical Skills: Windows 95/98/NT/2000/XP/Vista/7, MS Visio, MS Access, MS Word, MS Excel, MS Project, Rational Rose Enterprise, IBM Rational',
 'EXPERIENCE: eBay - Austin, TX Jan 2016 to Present Sr. Java Fullstack Developer The GSI [eBay] Remote Tools system is a browser-based',
 'TECHNICAL SKILLS Development / Service Technologies/ClientSide Technologies Java, J2EE (Servlets, JSP, JSP Custom Tags, JSF, AngularJS,',
 'PROFESSIONAL EXPERIENCE 1. Germania Insurance, Brenham, TX Sr. Business Systems Analyst Feb 2017- Till date The aim of the project to develop',
 'Scrum Master Certified #000727059 TECHNICAL SKILLS Programming Languages :Java, J2EE,SQL, PL/SQL,.Net Web Technologies :XML, CSS, JavaScript,',
 'PROFESSIONAL EXPERIENCE 1. Germania Insurance, Brenham, TX Sr. Business Systems Analyst Feb 2017- Till date The aim of the project to develop an Enterprise',
 'Skill Set: Languages Java, J2EE, C, C#,C++, ASP.Net, ADO.Net Technologies HTML5, CSS3, XML, Java Script, jQuery, Ajax, AngularJS, Node JS, Backbone JS,',
 'Technical Skills Management Tools JIRA, HP ALM (Quality Center), Rational DOORS, and SharePoint SDLC Techniques Waterfall, Agile, Scrum,',
 'Technical Skills: Core Java Collections, Generics, Multithreading, Serialization, Exception Handling, RMI, File I/O and Reflection',
 'Certification in Claims and Contract EPIC Certification in Tapestry Core BUSINESS & TECHNICAL SKILLS Software Tools:',
 'Certifications: Oracle Certified Associate (OCA) Sun Certified Java Developer (SCJD)',
 'Professional Experience: First Citizens Bank, Raleigh NC January 2017 to Present Role: Sr. Java/ J2EE develo',
 'Professional SCRUM Master (PSM) in agile methodology (2017) Scrum.org',
 'Professional Development Certified Professional Scrum Master (PSM) Certified Project Management Professional (PMP) Agile Project Management Cyber Security',
 'Certifications Six Sigma Green Belt Sun Certified Java Programmer Professional Scrum Master',
 'PROFESSIONAL EXPERIENCE: Client: Molina Healthcare, Long Beach, USA Duration:Jan 2016 – Till Date Role: Sr. Business Analyst Project Description',
 'Tools/Methods Databases Oracle 8i/9i/10g/11i, MS Access, MS SQL server 2000/2005, MySQL, DB2. Operating Systems UNIX, Windows 7/Vista/ XP/NT/2008/2003/2000',
 'PROJECTS SUMMARY: Integration System Testing of AT&T Mobile Apps on various mobile platforms Mar2013-Present Role: Project Manager Respons',
 'Achievements Received client appreciations for outstanding performance.',
 'Technical Skills Platforms: MS-DOS, Windows (95, 98, Me, NT, 2000, XP, Vista, 7), UNIX, Linux Methodologies/Process: SDLC, Waterfall, Agile, SCRUM, UML,',
 'Technical Skills Platforms: MS-DOS, Windows (95, 98, Me, NT, 2000, XP, Vista, 7), UNIX, Linux Methodologies/Process: SDLC, Waterfall, Agile, SCRUM, UML,',
 'CERTIFICATIONS Oracle Certified Java Programmer Oracle Certified Business Component Developer TECHNICAL SKILLS Methodologies Agile, Object Oriented',
 'Certifications: Project Management Institute www.pmi.org USA Project Management Professional (PMP®) (1851131) OCP (Oracle Certified Professional)',
 'Sun Certified Java Programmer. Technical Skills: Programming Languages: Java J2SE 1.6/1.7, J2EE, JSP, SQL Server 2005, Python, MySQL, Oracle,',
 'TECHNICAL EXPERTISE: Languages Java, SQL, PLSQL, C, C++, UML Frameworks Spring, Hibernate, IBATIS, Jakarta Struts 1.1, JPA, JSF.',
 'TECHNICAL SKILLS: Software development and management tools Eclipse, IBM RAD, JBuilder, NetBeans, MyEclipse, Dream Weaver, Surround SCM, Rational ClearCase,',
 'WORK EXPERIENCE Business Systems Analyst Sep 2015 – Present Client: Giant Eagle Location: Pittsburgh, PA Responsible for requirements gathering, analysis,',
 'Please do not hesitate to contact me if you require references from any of my previous employers.',
 'Role: Java Developer Project Description: Next Education Pvt. Ltd. is an e-learning company which develops online e-learning',
 'PROFESSIONAL EXPERIENCE: Client: Quality Software Services, Inc (QSSI), Columbia MD Feb 2017 - Present Role: Sr. Java/J2EE Developer',
 'Certified Scrum Master( CSM) Project Management Professional (PMP) Certification (In Progress) Project Management, E-commerce,',
 'Experience Fiserv Business System Analyst, Alpharetta(GA) May 2017 – Till Date Bill Matrix Next(BMX) BillMatrix Next gives consumers',
 'PROFESSIONAL EXPERIENCE Walgreens, Deerfield, IL (Wrking Remotely) June 2015- till date Lead Java J2EE Developer Helping to design, implement,',
 'Trained on Scaled Agile Framework (SAFe) 2015 BSI ISMS Auditor training 2012 Certifications: Certified Scrum Master from Scrum Alliance (CSM)',
 'Certified User Cisco Certified Network Associate (CCNA) Six Sigma Green Belt Certification PMI PMP Certified Information Technology Infrastructure Library',
 'CERTIFICATIONS: -Certified Scrum master PROFESSIONAL EXPERIENCE: CLIENT: KOHL’s, MENOMONEE FALLS-WI Jan2017-Present ROLE: Sr. Business System',
 "SKILLS Microsoft Project Server, Java, MS Visio, Sharepoint, PHP, MS Project, JIRA, Rally, Adobe Suite ADDITIONAL",
 'Certified Scrum Master® from Scrum Alliance License No: 000491155 Certified Scrum Product Owner® from Scrum Alliance License No: 0004911',
 'PMP®: Certified Project Management Professional from PMI active till 2019 CSM®: Certified Scrum Master from Scrum Alliance QTP/QC',
 'Certifications on QTP and QC (Quality Center) from HP ISTQB: Certification on Software Testing from ISTQB Trainings/Knowledge Base ITIL',
 "SKILLS CSS, HTML, JavaScript, MySql, PHP ADDITIONAL INFORMATION Technical Skills Programming PHP 4, PHP 5, PHP 7, Java Script,",
 'Honors 2016 “Live the Values” Delivery Hi-Tech Award by UST Global 2011,2012 “Xtra Mile” Champion Award by HCL Technologies 2010 “Outstanding Performer” Award by Belgaco',
 'PROFESSIONAL EXPERIENCE Client6 Sep 2016 – Till Date Fidelity National Information Services Inc.(FIS)',
 'Professional Experience: Verizon, TX. Dec’16 – Tilldate. Role : Java Developer. Description : Verizon DVS Migration(CPF, Reconciliation,',
 'Professional experience Sr. Java Developer PG & E - San Francisco, CA April 2015 to Present Responsibilities:',
 'Project Management Professional (PMP) – PMP® Certification # License Number: 1820042 Certified Scrum Master (CSM) – License # 000466935 CORE COMPETENCIES',
 'PROFESSIONAL EXPERIENCE: Cisco Systems ,SanJose,CA Sep 2016 - Dec 2017 Role: Java/J2EE IT - Technology Lead ( Full Stack Developer)',
 'PROFESSIONAL TRAINING AND CERTIFICATIONS: Lead SAFe Agilist ~ PMP ~ Scrum professional ~Java Sun certification (2000) ~ TQMI Internal Auditor IS0 9001:2000 ~ Soft skills development',
 'PROFESSIONAL EXPERIENCE: Client: NCR Corporation, Atlanta USA May, 2017 to till',
 '(Project Manager- DevOps Dec-2016 -Present. Responsible for COE Automation projects. Involving business meetings to get understand the new scope,',
 'TECHNICAL SKILLS Languages C, C++, Java, SQL, PL/SQL. Mark-up/XML Technologies HTML, CSS, XHTML, JavaScript, JQuery, Ajax, AngularJS, XML, XSD, XSL/XSLT,',
 'Certifications, AWARDS and Achievements: [1Z0-591] OBIEE 11g Administration [1Z0-133] Oracle WebLogic Server 12c: Admin',
 'SKILLS TESTING, SQL, JAVA, HTML, ORACLE ADDITIONAL INFORMATION TECHNICAL SKILLS Testing Tools Selenium 2 Web Driver/RC/IDE/Grid,',
 'Certified Project Management Professional (PMP) from PMI Certified SCRUM Master (CSM) from Scrum Alliance Advanced Diploma in RDBMS (Oracle)',
 'Certified Project Management Professional (PMP) from PMI Certified SCRUM Master (CSM) from Scrum Alliance Advanced Diploma in RDBMS (Oracle)',
 'MSIS PROJECTS: Project: Social Media Analytics (Tweets Data Mining) Social media (twitter) data analysis using text mining. We wanted to',
 'Environment\\Tools:MS Visio, MS Office Suite, SharePoint, Snagit',
 'PROFESSIONAL EXPERIENCE WFS, FL Aug 2015 – Present Senior Java Fullstack Developer World Fuel Service is a leading global fuel logistics company',
 'Technical Skills: Programming languages Java, C#, Ruby scripting Web Technologies HTML, CSS and JavaScript. Databases Oracle, Document DB',
 'CERTIFICATION Certified PRINCE2 Practitioner (APMG), ID: P2R/IN076432 Certified Managing Multiple Projects (AACPS Global), ID: 24719150705 Certified ITIL®v3F (APMG), ID: GR75',
 'Professional Experience: Client: GE Appliances, Louisville, KY Feb’16–Till date Role: Sr. Java Full stack Developer Description: This project',
 'Work Experience: McDonalds Corporation, Digital Applications, Chicago, IL Mar 2017 – Till date Senior Project',
 'ADDITIONAL INFORMATION: Methodologies used - Agile, DevOps, RESTful Web Services, Six Sigma, Rational Unified Process and Waterfall Modeling and Design Tools ',
 'Certifications: Scrum Master Certification from International Scrum Institute Professional Scrum master I (PSM I) Certification',
 'Certification: Scrum Master Accredited Certificat',
 'Technical Skills: Hadoop Core Services HDFS, Map Reduce, Hadoop YARN. Hadoop Distribution Cloudera. Hadoop Data Services Apache Hive, Pig, Sqoop, Flume.',
 'Black Belt Six Sigma Certified Green Belt Six Sigma Certified Project Management, American Project Management (APM), California OSHA Training, RIT',
 'TECHNICAL SKILLS: Languages Java 1.5/1.6/1.7/1.8, C, C++, SQL, PL/SQL Java Technologies EJB, JSP, Servlets, RMI, JDBC, JNDI, JMS, MQ Series, JNDI,',
 'Marketing, Digital, Healthcare, HCM, FSCM, etc. Effectively identify scope of improvement; strategize business processes, workflow design Possess SQL',
 'skills in various database models like Oracle 11g/9i/8i, MS SQL Server 2005 Proven ability to imply improved control points on organization data and',
 'PROFESSIONAL EXPERIENCE Raymond James, St. Petersburg, FL Jul 2016 to Present Sr. Business System Analyst Raymond James serves high net worth',
 'GNX Program from NIIT Limited in 2000 Oracle Development from Kashyaap Radiant Info School E-commerce from Software Solution Integrated Solutions Limited. ETL, Business Objects and',
 'Exports Sales and Content Management. Worked with reputed clients like Medco, Stanford, Deloitte and Open Text Corporation.',
 'Working knowledge of continuous integration and continuous deploy mechanism and technologies like Jenkins, Maven etc.',
 'Technical Skills Set: Web Languages Core Java, J2EE (JSP, JSF, Servlets, Struts 2.0, Spring Web Flow, JSP Tag Libraries, ',
 'CERTIFICATIONS: Success Factors – Employee Central TECHNICAL SKILL SET: Database Environments, ETL, Integration and Web services Oracle,',
 'CERTIFICATIONS Project Management professional (PMI-PMP), Certification in SCRUM Master (CSM) PROJECT DETAILS Migration of Integration',
 'Certified Scrum Master (PSM 1) (Scrum.org) https://www.scrum.org/certification-list # enter first and last name Certified Scrum Master',
 "SKILLS SQL, JAVA, INTEGRATION, INTEGRATOR, ORACLE ADDITIONAL INFORMATION Methodologies Agile (Scrum), Waterfall, SDLC, STLC",
 'Professional Experience Client: State Street Corporation. July 2015 - Till date Location: Boston, MA. Role: Java Full Stack Developer.',
 'WORK EXPERIENCE #1 SR. JAVA/UI DEVELOPER Sept’2015 – Till Date Client: Office of State Courts Administrator (OSCA), Jefferson City, MO.',
 'Work Experience : Ascension Health, St Louis, MO Aug 2017- Sep 2017 Business Analyst Responsibilities: Test Execution and Reporting of the status in daily',
 'PROFESSIONAL EXPERIENCE: Client: Anixter Inc, Glenview, IL Jan 2017 – Present Role: Sr. Java/UI Developer Description: My role in Information Services',
 'Certifications Microsoft Certified Professional (Windows Server 2003) Microsoft Certified Systems Administrator (Messaging on Windows Server 2003)',
 'Technical Skills: Languages Java , XML, XSL, C++, C, SQL, PL/SQL, HTML/DHTML J2EE Standards JDBC, JNDI, XML Deployment Descriptors. Web Technologies JSP,',
 'Technical Skills/Knowledge: Methodologies/Environments Waterfall Methodology, Agile Methodology, RUP-Rational Unified Process, Scrum Version Control Rational Clear Case,',

 'Cybrary Online Basic A+ Certification Prep Durham Technical Community College 80 Continuing Education Credits 34 College Credit Hours Central Texas College 1976',
 'Relevant Coursework: Information System Development | Information System Design | Data Management for Business Analytics | Data Analytics',
 'Certificate: (Information Security Systems- Secure Networking), (December, 2012) EXPERIENCE Toyota Financial Services,',
 'PROFESSIONAL ACCOMPLISHMENTS: State Street, Boston, MA Sep 18 – Current Jr. Project Manager/Program Coordinator Worked as',
 'EXPERIENCE Mastercard technology pvt. ltd. Pune, India Solutions Arc',
 'Bilingual: English/German Technical Skills: PMP certified Ability to interpret instructions from various formats. Microsoft Project ▪ Excel ▪ Outlook',
 '(September 2013 – Dec 2014) Agile certified Reference Upon request 1',
 'Scrum Master Certification Id: 01100109302438',
 'TECHNICAL SKILLS: Access/Identity Management IBM Security Identity Manager (ISIM v6.0) IBM Tivoli Directory Integrator (ITDI 7.1.1),',
 'Work Experience Quest Diagnostics - Dallas, TX December 2017 to Present Business Analyst Responsibilities:',
 'Certifications: Oracle Certified Expert Java EE6 Web Component Developer.',
 'Oracle Certified Professional Java SE6 programmer. Android Certified Professional.',
 'Technical Skills: Languages: Java, Android, IOS. Databases: Oracle. Web Server: Tomcat. JEE technologies: JSP, Servlets, JSTL, JSON, JNDI,',
 'Certification: SAP (SD & PM) Certified Scrum Master (CSM) ID# 000742976 U.S. VISA: H1b (Second extension) Valid till Aug 2020 with I-140',
 'TRAINING Certification in Database Design, Data Recovery, Focus Database Management PMP',
 'Certification – New Horizons – Raleigh 2008 1985',
 'CERTIFICATIONS:CompTIA Security + AWS Certified CISSP in Progress',
 'TECHNCIAL SKILLS: Penetration testing tools: Cain and Abel, John the Ripper Vulnerability scanning: Nmap, Rapid 7 Nexpose and Nessus',
 'IBM Leadership Excellence Program Project Management Orientation Project Management Fundamentals',
 'References: Provided upon request… (732) 734 - 0440 Page 1 of 4',
 "Accomplishments Designed and created interactive training presentation for an international launch of a large corporation's application.",
 'EXPERIENCE TransLoc (Acquired by Ford), Durham, NC 06/16 - Present Enterprise Agile Coach - People Operations Hire and manage Agile Coaches',
 'CERTIFICATION: Project Management Professional (PMP) Dec-2018 (Expected)',
 'Certified Programmer. Sun Certified Web Component Developer.',
 'Technical Skills: Analytics SQL, Python, SAS, Tableau, Power BI, SAP, PowerPoint, QlikView, Alteryx,',
 'COURSE WORK: Design and analysis of Algorithms; Data Structures using Java; Database Systems; Operating Systems;',
 'TECHNICAL SKILLS: Programming languages and Scripts Java 1.5/1.6/1.7/1.8, J2EE 1.4, Java Script, HTML, Angular JS',
 'CERTIFICATION Microsoft Certified Professional (MCP): Microsoft SQL Server 2012/2014. Certification Number: G148-0012. DAT201x:',
 "Certification Microsoft Certified Database Administrator (SQL Server 2000) Microsoft Certified Technology Specialist (SQL Server 2005)",
 'PROFESSIONAL EXPERIENCE Facebook California August 2016 – January 2019 The Facebook service can be accessed from devices with Internet',
 'Leadership Role Vice President of Finance, Alpha Kappa Psi Fall 2009 Semester Skills Computer',
 'TECHNICAL SKILLS: Testing Tools Selenium IDE, Selenium RC, Web driver,Grid,Protractor,Quick Test Pro,Appium(ios/Android)',
 'Certifications: Microsoft® Certified Technology Specialist: SQL Server 2008, Database Development Microsoft® Certified Professional',
 'Scrum Master Accreditation Certification (SMAC).',
 'Corporation, IL Sr .Net Developer Feb 2015 to Present Responsibilities: Worked closely with Client-side manager to understand long-term goals and',
 'Professional Experience: Travelport, LP Denver, CO August 2018- present Software Developer Project Description: Travelport,',
 'AWARDS World Bank, Washington, DC – 20 Year- Service of Distinction, Dedication and Commitment',
 'PROFESSIONAL EXPERIENCE Information System Security Officer (ISSO) – June 2018',
 'Technical Skills: Databases Tools SQL Enterprise Manager, Query Analyzer, SQL Server Management Studio, Business Intelligence Development Studio,',
 'Certifications OnBase Certified System Administrator Epic HIM Release of Information Certification (Epic 2018)',
 'M Rahman | Oracle DBA | Email: msdrahman8@gmail.com | Phone: 312-620-1219Page 4',
 'certifications Dorsey Business School - Diploma in Computer Operations',
 'Experience Wells Fargo, Charlotte, NC August 2017 – January 2019 Systems Analyst 3 Change Management and Incident',
 'HIGHLIGHTS: Successfully delivered more than 30 features (B2C and B2B) across platforms for various organizations.',
 'Professional Experience: COMCAST, Philadelphia, Pennsylvania AUG 2017 – Present Role: Workday Integration Developer',
 'Certifications Project Management Institute – Project Management Professional (PMP) Skills Communications',
 'Professional Experience Accenture Federal Services Reston VA Sr. Java/J2EE Developer Dec 2018 to Present Responsibilities',
 'CERTIFICATIONS: Certified Scrum Master (CSM®)from ScrumAlliance.org SAFe® 4.5 Agilist(SA) from SCALED AGILE Inc.',
 'Professional Experience Client: Verizon April’ 19 – present Role: Java Developer Location: Texas, Irving.',
 'Certifications Certified Information Systems Security Professional (CISSP) in progress,',
 'Certifications: ASTQB Certified tester Technical Skills Software Testing Functional Testing, Module Level Testing, Integration Testing, System',
 'SAP Certified Netweaver ABAP Development Consultant Advanced Diploma in System Management” certification from NIIT Bangalore Training Attended 5 days training',
 'Relevant Coursework: Operating System Design, Java Programming, Internet & Higher Layer Protocol, Software Architecture',
 'Work Experience: Amex, Pheniox, AZ Jul 17 - Present Senior QA Automation Test Engineer Responsibilities:',
 'Technical Skills: Domain Technologies Used Programming Languages: Java 1.8, 1.7, 1.6, C, C++ JAVA Enterprise Technologies:',
 'Certifications Certified in Scrum Agile Done Internship in SQL and SSIS development from iAppSoftSolutions Done certification',
 'Certifications / Trainings: CDCP – Certified Data Centre Professional –Security Checkpoint security administrator ITIL v3',
 'TECHNICAL PROFICIENCY: Browsers Internet Explorer 6,7,8,910 and 11, Chrome, Firefox, Safari Web Development Tools XML, HTML, Ajax',
 'CERTIFICATIONS AND AWARDS Sun Certified Java Professional ISTQB Certified SKILLS AND ABILITIES QA Automation',
 'WORK EXPERIENCE Commonwealth of Massachusetts - Department of Transitional Assistance. Boston, MA',
 'Technical Skills Methodologies : Waterfall, Agile-SCRUM Business Modeling Tools : MS VISIO, Rational Rose. Project Management',
 'Certificate in Networking and Distributed Systems – Columbus State Community College, Columbus, OH Certificate in Interconnecting Cisco',
 'Certified Cloud Security Knowledge (CCSK) CompTIA Security + CRISC in Progress CISSP in Progress TECHNCIAL SKILLS: Penetration testing',
 'Professional Experience: Client: Accenture Inc, Chicago Jun15- Current Role: Full Stack Web Developer Responsibilities',
 'TECHNICAL SKILLS: Networking Concepts: OSI Model, TCP/IP, UDP, IPV4, IPv6, Sub-netting, VLSM Routing Protocols',
 'Certifications: Certified Information Security Manager (CISM) Certified Information Systems Security Professional (CISSP) Microsoft Certified',
 'Certifications: Executive Data Science, Google Ads Video, Google Ads Digital Sales, Python, IBM Applied AI CORE',
 'TECHNICAL SKILLS Programming Languages Java, MATLAB, Python Java Technologies Core Java, Collections',
 'Certificate in Project Management University of Toronto Certified Project Management Professional (PMP® #462308) Project Management Institute Certified Scrum Master',
 'PROFESSIONAL EXPERIENCE Company Name: Pioneer Natural Resources Location: Irving, TX Sr Workday HCM Consultant',
 'WORK EXPERIENCE United Health Group, NY Sr. Business Data Analyst / Data Analyst 07/2018-Present Scope of this project was to migrate',
 'PROFESSIONAL EXPERIENCE AT & T, Oakton, VA GBSOMS Role: Sr. Java Developer June 2016 – Present Project Description: The Global Blue',
 'TECHNICAL SKILLS Operating System : Windows 7/ Vista/ XP, Windows 2003/ 2000/ NT, UNIX Programming Languages : C#.NET, VB.NET, SQL',
 'CERTIFICATIONS: CCNA® Certified REFERENCES: Available Upon Request PREFERRED LOCATIONS: Willing to relocate anywhere',
 'PROFESSIONAL EXPERIENCE: Fannie Mae, Herndon VA April 2016 – Till Date Project Name: Market Room Console QA Automation Tester',
 'Certificates PRINCE2® Foundation Certificate (2017) in Project Management ITIL Foundation Certificate in IT Service',
 'Professional Experience: Advance Auto Parts, Raleigh, NC June 2018 – Present Role: Network Engineer Responsibilities',
 'Certification: Cisco Certified Network Associate (CCNA) Cisco Certified Network Professional (CCNP)',
 'Certificate in Computer Programming and Systems Analysis Hobart College: B.A., English, minor in Anthropology OTHER EXPERIENCE VOLUNTEER Served as a mentor in the Big Brother program.',
 'Technical Skills: .NET Technologies: C,C++, C#.NET, ASP.NET, ADO.NET, Web Services,.NET Framework 1.1/2.0/3.0/3.5/4.0, ADO.NET',
 'Professional Experience TD Ameritrade Jan ’ 14 - Present Sr Solution Architect Responsibilities Responsible for overall leadership,',
 'PROFESSIONAL EXPERIENCE: IHG, Atlanta, GA Feb 2018 - Present Project: Policy Based Payments Role: Java/J2EE Developer Description: The primary',
 'TECHNICAL SKILL SET: Programming languages C#, LINQ, ADO.Net, PL-SQL, T-SQL, Core JAVA, C, C++, Entity Framework Web',
 'Certifications Certified Java Programmer CSTE Certified Tester Projects Experience Client : DTCC, Jersey City, NJ Role : SDET – Sr.',
 'WORK EXPERIENCE BRIGHTSTAR December 2017 – December 2018 Location: Libertyville, IL, USA. Role: Java developer, Support',
 'Technical Skills: Microsoft Technologies ASP.NET,ADO.NET, AJAX, LINQ, MVC, Entity Framework, NHibernate, Web',
 'Role: .NET Developer June 2017 - Present Responsibilities: Involved in analysis, design, and development and testing of',
 'PowerBI.com and PowerBI mobile',
 'Modeling Tools Rational Requisite Pro, HPALM, UML, MS Visio, BPMN, Rational Rose, Rational Clear Case Methodologies / Frameworks Agile, Waterfall Database MS SQL Server, Oracle, MYSQL. BI Reporting Tools Tableau, OBIEE, Microstrategy, SAP Crystal Reports, Cognos. ETL Tools IBM Data stage, Informatica Power Center, SSIS QA Testing Tools HP QC, Load Runner',
 'Tools/Methods Databases Oracle 8i/9i/10g/11i, MS Access, MS SQL server 2000/2005, MySQL, DB2. Operating Systems UNIX, Windows 7/Vista/ XP/NT/2008/2003/2000, Linux Business Modeling Tools Rational Suite- Rational Rose, Data Mapping, MS Office Suite-Visio/Word/PowerPoint/Project/Office/SharePoint/Excel/Outlook, Visual Paradi',
 'First Class Project Management Certification - Kent State University (PMP) Certified Scrum Master from Scum Alliance – CSM Certified Product Owner -CSPO Core Competency Technical Skill set Specialized Technology Mobility, Digital, SOA/Web services, IOS, Android, Mongo DB, Cloud- HP/VMware, and Data Analytics. Software Development technology Java, J2ee, EJB, JSP,',
 'Certifications: PMI - PMP, CSM (Certified Scrum Master), APICS – CSCP, LSSGB (Lean Six Sigma Green Belt) Professional Experience: Signet Jewelers, Fairlawn, OH',
 'TECHNICAL SKILLS Development / Service Technologies/ClientSide Technologies Java, J2EE (Servlets, JSP, JSP Custom Tags, JSF, AngularJS, JQuery HTML5, CSS3,JDBC, EJB, JNDI, RMI, JMS),SQL, Spring, Hibernate, SOAP and RestFul Web Services, Message Broker, Mule ESB, ILog JRules',
'PROFESSIONAL EXPERIENCE: Client: Molina Healthcare, Long Beach',
'TECHNICAL SKILLS Development / Service Technologies/ClientSide Technologies Java, J2EE (Servlets, JSP, JSP Custom Tags, JSF, AngularJS, JQuery HTML5, CSS3,JDBC, EJB, JNDI, RMI, JMS),SQL, Spring, Hibernate, SOAP and RestFul Web Services, Message Broker, Mule ESB, ILog JRules',
'Negotiate to Win | 7 Habits of Highly Effective People',
'TECHNICAL SKILLS Methodologies Agile, Object Oriented Analysis and Design, Gang of Four and J2EE Design Patterns, Domain Driven Design, Test Driven Development, Aspect Oriented Programming (AOP)',
'Certifications: Project Management Institute www.pmi.org USA Project Management Professional (PMP®) (1851131) OCP (Oracle Certified Professional) SCJP (Sun Certified Java Programmer)',
'Professional EXPERIENCE: GE Health Care, Barrington, IL Oct 15 – Till Date Project Manager',
'Current Location: Plano Texas Relocation: yes Currently on project: Office of Attorney Child Support System Availability to start for Project: 2 weeks Available for In-Person Interview (Yes OR No): yes Employer Contact Details: Manikanta Software Bloc LLC. AnE-verified Company',
'Black Belt Six Sigma Certified Green Belt Six Sigma Certified Project Management, American Project Management (APM), California OSHA Training, RIT (Rochester Institute of Technology): NewPage Company (10 Hours in-site).',
'PROFESSIONAL EXPERIENCE: Client: Anixter Inc, Glenview, IL Jan 2017 – Present Role: Sr. Java/UI Developer Description: My role in Information Services (IS Dept) is to analyze & resolve requirements for Order Entry (OE), Order Management (OM) and Store Front System. OE, OM & Store Front application is to help & guide sales',
'PROJECTS PROFILE Title Claims Processing Services Development Team Client Asurion Insurance , TN Position Associate – Software Team Size 10 Environment TIBCO BusinessWorks, TIBCO Administrator, TIBCO Hawk, TIBCO EMS O/s: Windows XP, Unix Duration Apr’11 – till date',

]


date_list = ['(Dec 2007)','June 2004 – April 2008','2001 – 2005','2007 - 2008','2013-2015','2016 - 2019','2011','2012','2013','2005','2008','2009','2010','2014','2015','2016','2017','2004','2001','jan - 1990', 'feb - 1991', 'mar - 1992', 'apr - 1993', 'aug - 1994', 'sept - 1995', 'nov - 1996', 'oct - 1997', 'dec - 1998', 'january - 1999', 'february - 2000', 'march - 2001', 'april - 2002', 'may - 2003', 'june - 2004', 'july - 2005', 'august - 2006', 'september - 2007', 'october - 2008', 'november - 2009', 'december - 2010', 'JAN - 2011', 'FEB - 2012', 'MAR - 2013', 'APR - 2014', 'AUG - 2015', 'SEPT - 2016', 'NOV - 2017', 'OCT - 2018', 'DEC - 2019', 'JANUARY - 2020', 'FEBRUARY - 2021', 'MARCH - 1990', 'APRIL - 1991', 'MAY - 1992', 'JUNE - 1993', 'JULY - 1994', 'AUGUST - 1995', 'SEPTEMBER - 1996', 'OCTOBER - 1997', 'NOVEMBER - 1998', 'DECEMBER - 1999', 'Jan - 2000', 'Feb - 2001', 'Mar - 2002', 'Apr - 2003', 'Aug - 2004', 'Sept - 2005', 'Nov - 2006', 'Oct - 2007', 'Dec - 2008', 'January - 2009', 'February - 2010', 'March - 2011', 'April - 2012', 'May - 2013', 'June - 2014', 'July - 2015', 'August - 2016', 'September - 2017', 'October - 2018', 'November - 2019', 'December - 2020','jan-1990', 'feb-1991', 'mar-1992', 'apr-1993', 'aug-1994', 'sept-1995', 'nov-1996', 'oct-1997', 'dec-1998', 'january-1999', 'february-2000', 'march-2001', 'april-2002', 'may-2003', 'june-2004', 'july-2005', 'august-2006', 'september-2007', 'october-2008', 'november-2009', 'december-2010', 'JAN-2011', 'FEB-2012', 'MAR-2013', 'APR-2014', 'AUG-2015', 'SEPT-2016', 'NOV-2017', 'OCT-2018', 'DEC-2019', 'JANUARY-2020', 'FEBRUARY-2021', 'MARCH-1990', 'APRIL-1991', 'MAY-1992', 'JUNE-1993', 'JULY-1994', 'AUGUST-1995', 'SEPTEMBER-1996', 'OCTOBER-1997', 'NOVEMBER-1998', 'DECEMBER-1999', 'Jan-2000', 'Feb-2001', 'Mar-2002', 'Apr-2003', 'Aug-2004', 'Sept-2005', 'Nov-2006', 'Oct-2007', 'Dec-2008', 'January-2009', 'February-2010', 'March-2011', 'April-2012', 'May-2013', 'June-2014', 'July-2015', 'August-2016', 'September-2017', 'October-2018', 'November-2019', 'December-2020','jan 1990', 'feb 1991', 'mar 1992', 'apr 1993', 'aug 1994', 'sept 1995', 'nov 1996', 'oct 1997', 'dec 1998', 'january 1999', 'february 2000', 'march 2001', 'april 2002', 'may 2003', 'june 2004', 'july 2005', 'august 2006', 'september 2007', 'october 2008', 'november 2009', 'december 2010', 'JAN 2011', 'FEB 2012', 'MAR 2013', 'APR 2014', 'AUG 2015', 'SEPT 2016', 'NOV 2017', 'OCT 2018', 'DEC 2019', 'JANUARY 2020', 'FEBRUARY 2021', 'MARCH 1990', 'APRIL 1991', 'MAY 1992', 'JUNE 1993', 'JULY 1994', 'AUGUST 1995', 'SEPTEMBER 1996', 'OCTOBER 1997', 'NOVEMBER 1998', 'DECEMBER 1999', 'Jan 2000', 'Feb 2001', 'Mar 2002', 'Apr 2003', 'Aug 2004', 'Sept 2005', 'Nov 2006', 'Oct 2007', 'Dec 2008', 'January 2009', 'February 2010', 'March 2011', 'April 2012', 'May 2013', 'June 2014', 'July 2015', 'August 2016', 'September 2017', 'October 2018', 'November 2019', 'December 2020']

edu_data1 = []          #i            #j            #k              #l
for i,j,k,l in zip(universitys*3, educations*20,garbage_data*58,date_list*71):
    x = random.choice(universitys)
    y = random.choice(educations)
    text1 = 'Education Documents'+ ' ' + j + ' ' + i + ' ' + l + ' ' + y + ' ' + x + ' ' + l + ' ' + k
    if ((i == x) and (j == y)):
      x = random.choice(universitys)
      y = random.choice(educations)

    elif ((i != x) and (j != y)):
      lenofedu = text1.find(j)
      posofedu = lenofedu + len(j)

      lenofuni = text1[posofedu:].find(i)+posofedu
      posofuni = lenofuni + len(i)

      lenofdate = text1[posofuni:].find(l)+posofuni
      posofdate = lenofdate + len(l)

      lenofedu1 = text1[posofdate:].find(y)+posofdate
      posofedu1 = lenofedu1 + len(y)

      lenofuni1 = text1[posofedu1:].find(x)+posofedu1
      posofuni1 = lenofuni1 + len(x)

      lenofdate1 = text1[posofuni1:].find(l)+posofuni1
      posofdate1 = lenofdate1 + len(l)

      edu_pos = (lenofedu, posofedu, 'QUALIFICATION')
      uni_pos = (lenofuni, posofuni, 'UNIVERSITY')
      date_pos = (lenofdate, posofdate, 'DATE')
      edu_pos1 = (lenofedu1, posofedu1, 'QUALIFICATION')
      uni_pos1 = (lenofuni1, posofuni1, 'UNIVERSITY')
      date_pos1 = (lenofdate1, posofdate1, 'DATE')
      data = (text1, {"entities": [edu_pos, uni_pos, date_pos, edu_pos1, uni_pos1, date_pos1]})
      edu_data1.append(data)

edu_data2 = []
for i,j,k,l in zip(universitys, educations*20,garbage_data*58,date_list*71):
    x = random.choice(universitys)
    y = random.choice(educations)

    text2 = 'Educational Qualification:'+ ' ' + j + ' ' + 'from' + ' ' + i + ' ' + l + ' ' + y + ' ' + 'from' + ' ' + x + ' ' + l + ' ' + k
    if ((i == x) and (j == y)):
      x = random.choice(universitys)
      y = random.choice(educations)

    elif ((i != x) and (j != y)):
      lenofedu = text2.find(j)
      posofedu = lenofedu + len(j)

      lenofuni = text2[posofedu:].find(i)+posofedu
      posofuni = lenofuni + len(i)

      lenofdate = text2[posofuni:].find(l)+posofuni
      posofdate = lenofdate + len(l)

      lenofedu1 = text2[posofuni:].find(y)+posofuni
      posofedu1 = lenofedu1 + len(y)

      lenofuni1 = text2[posofedu1:].find(x)+posofedu1
      posofuni1 = lenofuni1 + len(x)

      lenofdate1 = text2[posofuni1:].find(l)+posofuni1
      posofdate1 = lenofdate1 + len(l)

      edu_pos = (lenofedu, posofedu, 'QUALIFICATION')
      uni_pos = (lenofuni, posofuni, 'UNIVERSITY')
      date_pos = (lenofdate, posofdate, 'DATE')
      edu_pos1 = (lenofedu1, posofedu1, 'QUALIFICATION')
      uni_pos1 = (lenofuni1, posofuni1, 'UNIVERSITY')
      date_pos1 = (lenofdate1, posofdate1, 'DATE')
      data = (text2, {"entities": [edu_pos, uni_pos, date_pos, edu_pos1, uni_pos1, date_pos1]})
      edu_data2.append(data)

edu_data3 = []
for i,j,k in zip(universitys, educations*20, garbage_data*58):
    x = random.choice(universitys)
    y = random.choice(educations)

    text3 = 'Educational Background:'+ ' ' + j + ' ' + 'at' + ' ' + i + ' ' + y + ' ' + 'at' + ' ' + x + ' ' + k
    if ((i == x) and (j == y)):
      x = random.choice(universitys)
      y = random.choice(educations)

    elif ((i != x) and (j != y)):
      lenofedu = text3.find(j)
      posofedu = lenofedu + len(j)

      lenofuni = text3[posofedu:].find(i)+posofedu
      posofuni = lenofuni + len(i)

      lenofedu1 = text3[posofuni:].find(y)+posofuni
      posofedu1 = lenofedu1 + len(y)

      lenofuni1 = text3[posofedu1:].find(x)+posofedu1
      posofuni1 = lenofuni1 + len(x)

      edu_pos = (lenofedu, posofedu, 'QUALIFICATION')
      uni_pos = (lenofuni, posofuni, 'UNIVERSITY')
      edu_pos1 = (lenofedu1, posofedu1, 'QUALIFICATION')
      uni_pos1 = (lenofuni1, posofuni1, 'UNIVERSITY')
      data = (text3, {"entities": [edu_pos, uni_pos, edu_pos1, uni_pos1]})
      edu_data3.append(data)

edu_data4 = []
for i,j,k,l in zip(universitys, educations*20,garbage_data*58,date_list*71):
    x = random.choice(universitys)
    y = random.choice(educations)

    text4 = 'EDUCATION'+ ' ' + j + ' - ' + i + ' ' + l + ' ' + y + ' - ' + x + ' ' + l + ' ' + k
    if ((i == x) and (j == y)):
      x = random.choice(universitys)
      y = random.choice(educations)

    elif ((i != x) and (j != y)):
      lenofedu = text4.find(j)
      posofedu = lenofedu + len(j)

      lenofuni = text4[posofedu:].find(i)+posofedu
      posofuni = lenofuni + len(i)

      lenofdate = text4[posofuni:].find(l)+posofuni
      posofdate = lenofdate + len(l)

      lenofedu1 = text4[posofdate:].find(y)+posofdate
      posofedu1 = lenofedu1 + len(y)

      lenofuni1 = text4[posofedu1:].find(x)+posofedu1
      posofuni1 = lenofuni1 + len(x)

      lenofdate1 = text4[posofuni1:].find(l)+posofuni1
      posofdate1 = lenofdate1 + len(l)

      edu_pos = (lenofedu, posofedu, 'QUALIFICATION')
      uni_pos = (lenofuni, posofuni, 'UNIVERSITY')
      date_pos = (lenofdate, posofdate, 'DATE')
      edu_pos1 = (lenofedu1, posofedu1, 'QUALIFICATION')
      uni_pos1 = (lenofuni1, posofuni1, 'UNIVERSITY')
      date_pos1 = (lenofdate1, posofdate1, 'DATE')
      data = (text4, {"entities": [edu_pos, uni_pos, date_pos, edu_pos1, uni_pos1, date_pos1]})
      edu_data4.append(data)

edu_data5 = []
for i,j,k,l in zip(universitys, educations*20,garbage_data*58,date_list*71):
    x = random.choice(universitys)
    y = random.choice(educations)

    text5 = 'Academics'+ ' ' + j + ' ' + i + ' - ' + l + ' ' + y + ' ' + x + ' - ' + l + ' ' + k
    if ((i == x) and (j == y)):
      x = random.choice(universitys)
      y = random.choice(educations)

    elif ((i != x) and (j != y)):
      lenofedu = text5.find(j)
      posofedu = lenofedu + len(j)

      lenofuni = text5[posofedu:].find(i)+posofedu
      posofuni = lenofuni + len(i)

      lenofdate = text5[posofuni:].find(l)+posofuni
      posofdate = lenofdate + len(l)

      lenofedu1 = text5[posofdate:].find(y)+posofdate
      posofedu1 = lenofedu1 + len(y)

      lenofuni1 = text5[posofedu1:].find(x)+posofedu1
      posofuni1 = lenofuni1 + len(x)

      lenofdate1 = text5[posofuni1:].find(l)+posofuni1
      posofdate1 = lenofdate1 + len(l)

      edu_pos = (lenofedu, posofedu, 'QUALIFICATION')
      uni_pos = (lenofuni, posofuni, 'UNIVERSITY')
      date_pos = (lenofdate, posofdate, 'DATE')
      edu_pos1 = (lenofedu1, posofedu1, 'QUALIFICATION')
      uni_pos1 = (lenofuni1, posofuni1, 'UNIVERSITY')
      date_pos1 = (lenofdate1, posofdate1, 'DATE')
      data = (text5, {"entities": [edu_pos, uni_pos, date_pos, edu_pos1, uni_pos1, date_pos1]})
      edu_data5.append(data)

edu_data6 = []
for i,j,k,l in zip(universitys, educations*20,garbage_data*58,date_list*71):
    x = random.choice(universitys)
    y = random.choice(educations)
    i=i
    j=j
    text6 = 'Academic Education & Certification'+ ' ' + j + ', ' + i + ' ' + y + ', ' + x + ' ' + k
    if ((i == x) and (j == y)):
      x = random.choice(universitys)
      y = random.choice(educations)

    elif ((i != x) and (j != y)):
      lenofedu = text6.find(j)
      posofedu = lenofedu + len(j)

      lenofuni = text6[posofedu:].find(i)+posofedu
      posofuni = lenofuni + len(i)

      lenofedu1 = text6[posofuni:].find(y)+posofuni
      posofedu1 = lenofedu1 + len(y)

      lenofuni1 = text6[posofedu1:].find(x)+posofedu1
      posofuni1 = lenofuni1 + len(x)

      edu_pos = (lenofedu, posofedu, 'QUALIFICATION')
      uni_pos = (lenofuni, posofuni, 'UNIVERSITY')
      edu_pos1 = (lenofedu1, posofedu1, 'QUALIFICATION')
      uni_pos1 = (lenofuni1, posofuni1, 'UNIVERSITY')
      data = (text6, {"entities": [edu_pos, uni_pos, edu_pos1, uni_pos1]})
      edu_data6.append(data)

edu_data7 = []
for i,j,k,l in zip(universitys, educations*20,garbage_data*58,date_list*71):
    x = random.choice(universitys)
    y = random.choice(educations)
    i=i
    j=j
    text7 = 'Academic Education & Certification'+ ' ' + j + ' ' + l + ' ' + i + ' ' + y +' '+ l + ' ' + x + ' ' + k
    if ((i == x) and (j == y)):
      x = random.choice(universitys)
      y = random.choice(educations)

    elif ((i != x) and (j != y)):
      lenofedu = text7.find(j)
      posofedu = lenofedu + len(j)

      lenofdate = text7[posofedu:].find(l)+posofedu
      posofdate = lenofdate + len(l)

      lenofuni = text7[posofdate:].find(i)+posofdate
      posofuni = lenofuni + len(i)

      lenofedu1 = text7[posofuni:].find(y)+posofuni
      posofedu1 = lenofedu1 + len(y)

      lenofdate1 = text7[posofedu1:].find(l)+posofedu1
      posofdate1 = lenofdate1 + len(l)

      lenofuni1 = text7[posofdate1:].find(x)+posofdate1
      posofuni1 = lenofuni1 + len(x)

      edu_pos = (lenofedu, posofedu, 'QUALIFICATION')
      date_pos = (lenofdate, posofdate, 'DATE')
      uni_pos = (lenofuni, posofuni, 'UNIVERSITY')
      edu_pos1 = (lenofedu1, posofedu1, 'QUALIFICATION')
      date_pos1 = (lenofdate1, posofdate1, 'DATE')
      uni_pos1 = (lenofuni1, posofuni1, 'UNIVERSITY')
      data = (text7, {"entities": [edu_pos, date_pos, uni_pos, edu_pos1, date_pos1, uni_pos1]})
      edu_data7.append(data)

edu_data8 = []
for i,j,k,l in zip(universitys, educations*20,garbage_data*58,date_list*71):
    x = random.choice(universitys)
    y = random.choice(educations)

    text8 = 'Education Details:-'+ ' ' + i + ', ' + j + ' - ' + l + ' ' + x + ', ' + y + ' - ' + l + ' ' + k
    if ((i == x) and (j == y)):
      x = random.choice(universitys)
      y = random.choice(educations)

    elif ((i != x) and (j != y)):
      lenofuni = text8.find(i)
      posofuni = lenofuni + len(i)

      lenofedu = text8[posofuni:].find(j)+posofuni
      posofedu = lenofedu + len(j)

      lenofdate = text8[posofedu:].find(l)+posofedu
      posofdate = lenofdate + len(l)

      lenofuni1 = text8[posofedu:].find(x)+posofedu
      posofuni1 = lenofuni1 + len(x)

      lenofedu1 = text8[posofuni1:].find(y)+posofuni1
      posofedu1 = lenofedu1 + len(y)

      lenofdate1 = text8[posofedu1:].find(l)+posofedu1
      posofdate1 = lenofdate1 + len(l)

      edu_pos = (lenofedu, posofedu, 'QUALIFICATION')
      uni_pos = (lenofuni, posofuni, 'UNIVERSITY')
      date_pos = (lenofdate, posofdate, 'DATE')
      edu_pos1 = (lenofedu1, posofedu1, 'QUALIFICATION')
      uni_pos1 = (lenofuni1, posofuni1, 'UNIVERSITY')
      date_pos1 = (lenofdate1, posofdate1, 'DATE')
      data = (text8, {"entities": [ uni_pos, edu_pos, date_pos, uni_pos1, edu_pos1, date_pos1]})
      edu_data8.append(data)

edu_data9 = []
for i,j,k,l in zip(universitys, educations*20,garbage_data*58,date_list*71):
    x = random.choice(universitys)
    y = random.choice(educations)

    text9 = 'EDUCATION/CERTICATIONS/TRAINING:'+ ' ' + i + ' - ' + l + ' - ' + j + ' ' + x + ' - ' + l + ' - ' + y + ' ' + k
    if ((i == x) and (j == y)):
      x = random.choice(universitys)
      y = random.choice(educations)

    elif ((i != x) and (j != y)):
      lenofuni = text9.find(i)
      posofuni = lenofuni + len(i)

      lenofdate = text9[posofuni:].find(l)+posofuni
      posofdate = lenofdate + len(l)

      lenofedu = text9[posofuni:].find(j)+posofuni
      posofedu = lenofedu + len(j)

      lenofuni1 = text9[posofedu:].find(x)+posofedu
      posofuni1 = lenofuni1 + len(x)

      lenofdate1 = text9[posofuni1:].find(l)+posofuni1
      posofdate1 = lenofdate1 + len(l)

      lenofedu1 = text9[posofdate1:].find(y)+posofdate1
      posofedu1 = lenofedu1 + len(y)

      edu_pos = (lenofedu, posofedu, 'QUALIFICATION')
      date_pos = (lenofdate, posofdate, 'DATE')
      uni_pos = (lenofuni, posofuni, 'UNIVERSITY')
      edu_pos1 = (lenofedu1, posofedu1, 'QUALIFICATION')
      date_pos1 = (lenofdate1, posofdate1, 'DATE')
      uni_pos1 = (lenofuni1, posofuni1, 'UNIVERSITY')
      data = (text9, {"entities": [uni_pos, date_pos, edu_pos, uni_pos1, date_pos1, edu_pos1]})
      edu_data9.append(data)

edu_data10 = []
for i,j,k in zip(universitys, educations*20,garbage_data*58):
    x = random.choice(universitys)
    y = random.choice(educations)
    i=i
    j=j
    text10 = 'EDUCATION/CERTICATIONS/TRAINING:'+ ' ' + i + ', ' + j + ' ' + x + ', ' + y + ' ' + k
    if ((i == x) and (j == y)):
      x = random.choice(universitys)
      y = random.choice(educations)

    elif ((i != x) and (j != y)):
      lenofuni = text10.find(i)
      posofuni = lenofuni + len(i)

      lenofedu = text10[posofuni:].find(j)+posofuni
      posofedu = lenofedu + len(j)

      lenofuni1 = text10[posofedu:].find(x)+posofedu
      posofuni1 = lenofuni1 + len(x)

      lenofedu1 = text10[posofuni1:].find(y)+posofuni1
      posofedu1 = lenofedu1 + len(y)

      edu_pos = (lenofedu, posofedu, 'QUALIFICATION')
      uni_pos = (lenofuni, posofuni, 'UNIVERSITY')
      edu_pos1 = (lenofedu1, posofedu1, 'QUALIFICATION')
      uni_pos1 = (lenofuni1, posofuni1, 'UNIVERSITY')
      data = (text10, {"entities": [uni_pos, edu_pos, uni_pos1, edu_pos1]})
      edu_data10.append(data)

edu_data11 = []
for i,j,k in zip(universitys, educations*20,garbage_data*58):
    x = random.choice(universitys)
    y = random.choice(educations)
    i=i
    j=j
    text11 = 'EDUCATION/CERTICATIONS/TRAINING:'+ ' ' + i + ' ' + j + ' ' + x + ' ' + y + ' ' + k
    if ((i == x) and (j == y)):
      x = random.choice(universitys)
      y = random.choice(educations)

    elif ((i != x) and (j != y)):
      lenofuni = text11.find(i)
      posofuni = lenofuni + len(i)

      lenofedu = text11[posofuni:].find(j)+posofuni
      posofedu = lenofedu + len(j)

      lenofuni1 = text11[posofedu:].find(x)+posofedu
      posofuni1 = lenofuni1 + len(x)

      lenofedu1 = text11[posofuni1:].find(y)+posofuni1
      posofedu1 = lenofedu1 + len(y)

      edu_pos = (lenofedu, posofedu, 'QUALIFICATION')
      uni_pos = (lenofuni, posofuni, 'UNIVERSITY')
      edu_pos1 = (lenofedu1, posofedu1, 'QUALIFICATION')
      uni_pos1 = (lenofuni1, posofuni1, 'UNIVERSITY')
      data = (text11, {"entities": [uni_pos, edu_pos, uni_pos1, edu_pos1]})
      edu_data11.append(data)

train_data = edu_data1[0:int(len(edu_data1)*.8)] + edu_data2[0:int(len(edu_data2)*.8)] + edu_data3[0:int(len(edu_data3)*.8)] + edu_data4[0:int(len(edu_data4)*.8)] + edu_data5[0:int(len(edu_data5)*.8)] + edu_data6[0:int(len(edu_data6)*.8)] + edu_data7[0:int(len(edu_data7)*.8)] + edu_data8[0:int(len(edu_data8)*.8)] + edu_data9[0:int(len(edu_data9)*.8)] + edu_data10[0:int(len(edu_data10)*.8)] + edu_data11[0:int(len(edu_data11)*.8)]
test_data = edu_data1[int(len(edu_data1)*.8):] + edu_data2[int(len(edu_data2)*.8):] + edu_data3[int(len(edu_data3)*.8):] + edu_data4[int(len(edu_data4)*.8):] + edu_data5[int(len(edu_data5)*.8):] + edu_data6[int(len(edu_data6)*.8):] + edu_data7[int(len(edu_data7)*.8):] + edu_data8[int(len(edu_data8)*.8):] + edu_data9[int(len(edu_data9)*.8):] + edu_data10[int(len(edu_data10)*.8):] + edu_data11[int(len(edu_data11)*.8):]


# print(len(train_data))
# print(len(test_data))

train_data = train_data[0:100]
test_data = test_data[0:40]

# Convert to train.spacy and dev.spacy

import pandas as pd
import os
from tqdm import tqdm
import spacy
from spacy.tokens import DocBin

nlp = spacy.load("en_core_web_sm") # load other spacy model
db = DocBin() # create a DocBin object
for text, annot in tqdm(train_data): # data in previous format
    doc = nlp.make_doc(text) # create doc object from text
    ents = []
    for start, end, label in annot["entities"]: # add character indexes
        span = doc.char_span(start, end, label=label, alignment_mode="contract")
        if span is None:
            print("Skipping entity")
        else:
            ents.append(span)

    doc.ents = ents # label the text with the ents
    db.add(doc)
# os.chdir(r'C:\XSeed_ML\XSeed_Edu_Code\corpus')
db.to_disk(r".\corpus\train.spacy") # save the docbin object

db = DocBin() # create a DocBin object
for text, annot in tqdm(test_data): # data in previous format
    doc = nlp.make_doc(text) # create doc object from text
    ents = []
    for start, end, label in annot["entities"]: # add character indexes
        span = doc.char_span(start, end, label=label, alignment_mode="contract")
        if span is None:
            print("Skipping entity")
        else:
            ents.append(span)
    doc.ents = ents # label the text with the ents
    db.add(doc)
# os.chdir(r'C:\XSeed_ML\XSeed_Edu_Code\corpus')
db.to_disk(".\corpus\dev.spacy") # save the docbin object